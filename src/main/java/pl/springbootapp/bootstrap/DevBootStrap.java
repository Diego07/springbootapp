package pl.springbootapp.bootstrap;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import pl.springbootapp.model.Author;
import pl.springbootapp.model.Book;
import pl.springbootapp.model.Publisher;
import pl.springbootapp.repositories.AuthorRepository;
import pl.springbootapp.repositories.BookRepository;
import pl.springbootapp.repositories.PublisherRepository;


/**
 * Created by Diego07 on 2017-11-04.
 */
@Component
public class DevBootStrap implements ApplicationListener<ContextRefreshedEvent> {

    private AuthorRepository authorRepository;
    private BookRepository bookRepository;
    private PublisherRepository publisherRepository;

    public DevBootStrap(AuthorRepository authorRepository, BookRepository bookRepository, PublisherRepository publisherRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        this.publisherRepository = publisherRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData() {

        Publisher publisher = new Publisher();
        publisher.setName("foo");

        publisherRepository.save(publisher);

        Author eric = new Author("Eric", "Evans");
        Book dddd = new Book("Driven Design", "1234", publisher);
        eric.getBooks().add(dddd);
        dddd.getAuthors().add(eric);

        authorRepository.save(eric);
        bookRepository.save(dddd);


        Author rod = new Author("Rod", "Johnson");
        Book noEJB = new Book("J2EE developmnent", "2344", publisher);
        rod.getBooks().add(noEJB);

        authorRepository.save(rod);
        bookRepository.save(noEJB);
    }
}
