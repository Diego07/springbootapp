package pl.springbootapp.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.springbootapp.model.Book;


/**
 * Created by Diego07 on 2017-11-04.
 */
public interface BookRepository extends CrudRepository<Book, Long> {


}
