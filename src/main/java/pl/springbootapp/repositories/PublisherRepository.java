package pl.springbootapp.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.springbootapp.model.Publisher;

/**
 * Created by Diego07 on 2017-11-04.
 */
public interface PublisherRepository extends CrudRepository<Publisher, Long> {
}
